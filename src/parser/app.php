<?php
/* -/\/\-\/\/- */

// include_once('errors.php');
require_once('errors.php');

// include_once('analyzer.php');
require_once('analyzer.php');

// include_once('xmlgenerator.php');
require_once('xmlgenerator.php');


/**
 * Aplikace pracuje jako filtr načte ze standardního vstupu zdrojový kód v IPPcode18, zkontroluje lexikální a syntaktickou správnost kódu a vypíše na standardní výstup XML.
 */
class App {
  /**
   * Uchovava instanci Analyzer, ktery je pouzit pro overovani
   * lexikalni a syntakticke spravnosti dle jazyka IPPcode18.
   * @var Analyzer
   */
  private $analyzer = null;
  /**
   * Uchovava instanci XMLGenerator, ktery je pouzit pro generovani
   * XML vystupu pro jazyk IPPcode18.
   * @var XMLGenerator
   */
  private $generator = null;
  /**
   * Uchovava pole argumentu, ktere odpovidaji argumentum na prikazove radce.
   * @var array
   */
  private $args = array();

  /**
   * Konstruktor
   * @param array $args Pole argumentu prikazove radky. Dle techto argumentu se nastavi vlastnosti. Ocekava se $argv ci shodny format.
   */
  function __construct($args = array()) {
    $this->analyzer = new Analyzer();
    $this->generator = new XMLGenerator();
    $this->args = $args;
  }

  /**
   * Funkce znormalizuje format argumentu prikazove radky.
   * Odstrani nazev programu (index 0), prevede ['--xxx=aaa'] na ['--xxx', 'aaa']
   * @param array $args Pole argumentu prikazove radky. Ocekava se $argv ci shodny format.
   * @return array Vraci upravene pole, ktere obsahuje jen argumenty a hodnoty.
   */
  public function NormalizeOpts($args = array()) {
    $out_args = array();
    foreach ($this->args as $key => $value) {
      if($key != 0) {
        if( preg_match("/^(--|-)/",$value) ) {
          $arr = preg_split("/=/",$value,2);
          array_push($out_args, $arr[0]);
          if( @$arr[1] )
            array_push($out_args, $arr[1]);
        }
        else {
          array_push($out_args, $value);
        }
      }
    } // foreach
    return $out_args;
  }

  /**
   * Funkce kontroluje zda se v poly vystytuji duplicitni hodnoty.
   * @param array $args Pole, ktere se bude kontrolovat na duplicitni hodnoty.
   * @return bool Vraci true pokud se v poly vyskytuji duplicitne nektere hodnoty. Jinak false, kdyz kazda hodnota je v poly maximalne jednou.
   */
  public function CheckDuplicity($args = array()) {
    $valid_args = array();

    foreach ($args as $key => $value) {
      if( in_array($value, $valid_args) ) {
        return true;
      }
      array_push($valid_args,"$value");
    } // foreach
    return false;
  }

  /**
   * Hlavni funkce, ktera spusti aplikaci.
   * Zpracuje svoje argumenty zadane pri vytvareni a dle jejich hodnot upravi chovani aplikace.
   */
  public function Main() {
    $args = $this->NormalizeOpts($this->args);

    if($this->CheckDuplicity($args)) {
      error_log("Byly zadany nektere argumeny duplicitne.\n");
      exit(Errors::ERR_INPUT_PARAM);
    }

    if( count($args) == 0 ) {
      $this->Compile();
      $this->PrintCode();
    }
    else if( in_array("--help", $args) ) {
      if( count($args) != 1 ) {
        error_log("Argument '--help' nelze kombinovat s jinymi argumenty.\n");
        exit(Errors::ERR_INPUT_PARAM);
      }
      $this->Help();
    }
    else if( in_array("--stats", $args) ) {
      $format = "";
      $count = 1;
      $next_stats_path = false;
      $path = "";

      foreach ($args as $key => $value) {
        if( $next_stats_path ) {
          $path = $value;
          $next_stats_path = false;
          $count++;
        }
        else if( $value == "--loc" ) {
          $format .= "L\n";
          $count++;
        }
        else if( $value == "--comments" ) {
          $format .= "C\n";
          $count++;
        }
        else if( $value == "--stats" ) {
          $next_stats_path = true;
        }
      } // foreach

      if( $format == "" ) {
        error_log("Argument '--stats' nelze pouzit bez '--loc' nebo '--comments'.\n");
        exit(Errors::ERR_INPUT_PARAM);
      }
      else if( count($args) != $count ) {
        error_log("Argument '--stats' lze kombinovat pouze s '--loc' a '--comments'.\n");
        exit(Errors::ERR_INPUT_PARAM);
      }

      $this->Compile();

      // Vypis statistik
      if($format) {
        $file_stats = @fopen("$path", "w");
        if( !$file_stats ) {
          error_log("Nepodarilo se vytvorit/prepsat soubor '$path' pro zapis statistik.\n");
          exit(Errors::ERR_OUTPUT_FILE);
        } 
        else {
          $count = $this->analyzer->comments_count;
          $format = preg_replace("/C/", "$count", $format);
          $count = $this->analyzer->commands_count;
          $format = preg_replace("/L/", "$count", $format);
          fwrite($file_stats, $format);
          fclose($file_stats);
        }
      }

      $this->PrintCode();
    }
    else {
      error_log("Zadany chybne argumenty.\nPomoci argumentu '--help' si vypises napovedu.");
      exit(Errors::ERR_INPUT_PARAM);
    }

  }

  /**
   * Funkce vypise napovedu k aplikaci na standartni vystup.
   */
  public function Help() {
    $name = $this->args[0];

    echo "Popis: \n";
    echo " Skrypt zpracovava kod IPPcode18 ze standartniho vstupu, provede lexikalni a syntaktickou kontrolu,a vygeneruje z nej XML dokument.\n";
    echo "\n";

    echo "Pouziti: '$name [prepinace]'\n";
    echo "\n";

    echo "Prepinace: \n";
    echo " --help\t\tVypise napovedu k programu.\n";
    echo " --stats=file\tDo souboru 'file' se ulozi zvolene statistiky.\n";
    echo " \t\tPrepinas se musi zkobinovat s '--loc' ci '--comments'\n";
    echo " --loc\t\tUlozi se do statistik pocet zpracovanych instrukci.\n";
    echo " --comments\tUlozi se do statistik pocet zpracovanych komentaru.\n";
    echo "\n";

    echo "Kody chyb: \n";
    echo "  10\tChybne zadane prepinace/argumenty nebo chybna kombinace.\n";
    echo "  12\tChyba pri otevirani/vytvareni souboru pro zapis statistik.\n";
    echo "  21\tDoslo k syntaktice/lexikalni chybe ve vstupnim kodu IPPcode18.\n";
    echo "  99\tInterni chyba.\n";
    echo "\n";
  }
  /**
   * Funkce zpracovava vstupni IPPcode18 a prevadi jej na XML format.
   */
  public function Compile() {

    $err = 0;

    // Kontrola hlavicky vstupu
    $this->analyzer->CheckHeader($err);
    if($err) { exit($err); }

    // Zpracovavani kodu
    while(!$this->analyzer->feof) {
      $val = $this->analyzer->GetTokens($err);
      if($err) { exit($err); }

      if($val != null) {
        $this->generator->AddInstruction($val[0],@$val[1][0],@$val[1][1],@$val[2][0],@$val[2][1],@$val[3][0],@$val[3][1]);
      }

    }
  }

  /**
   * Vytiskne na standartni vystup vygenerovany XML dokument.
   * @param boolean $formatOutput Urcuje zda bude vytisknut krasne formatovany vystup (true) nebo jednoradkovy zhusteny vypis (false).
   */
  public function PrintCode($formatOutput = false) {
    $this->generator->PrintXML($formatOutput);
  }

}// class App

//$app = new App($argv);
//$app->Main();


?>
