<?php
/* -/\/\-\/\/- */

/**
 * Trida obstaravajici promene a funkce pro kontrolu instrukci a jejich argumentu.
 */
abstract class Checker {

  /**
   * Obsahuje seznam instrukci, ktere nemaji zadne argumenty.
   * 
   * @var array
   */
  public static $inst = array("CREATEFRAME","PUSHFRAME","POPFRAME","RETURN","BREAK",
    "ADDS","SUBS","MULS","IDIVS","LTS","GTS","EQS","ANDS","ORS","NOTS","INT2CHARS","STRI2INTS"
    );
  /**
   * Obsahuje seznam instrukci, ktere maji jeden argument a to promennou.
   * 
   * @var array
   */
  public static $inst_var = array("DEFVAR","POPS");
  /**
   * Obsahuje seznam instrukci, ktere maji jeden argument a to promennou nebo konstantu.
   * 
   * @var array
   */
  public static $inst_symb = array("PUSHS","WRITE","DPRINT");
  /**
   * Obsahuje seznam instrukci, ktere maji jeden argument a to navesti.
   * 
   * @var array
   */
  public static $inst_label = array("CALL","LABEL","JUMP");
  /**
   * Obsahuje seznam instrukci, ktere maji dva argumenty. 
   * Prvni je promena a druhy je promenna nebo konstanta.
   * 
   * @var array
   */
  public static $inst_var_symb = array("MOVE","NOT","INT2CHAR","STRLEN","TYPE");
  /**
   * Obsahuje seznam instrukci, ktere maji dva argumenty. 
   * Prvni je promena a druhy je datovy typ.
   * 
   * @var array
   */
  public static $inst_var_type = array("READ");
  /**
   * Obsahuje seznam instrukci, ktere maji tri argumenty. 
   * Prvni je promena, druhy je promenna nebo konstanta a treti je promenna nebo konstanta.
   * 
   * @var array
   */
  public static $inst_var_symb_symb = array("ADD","SUB","MUL","IDIV","LT","GT","EQ","AND","OR","STR2INT","CONCAT","GETCHAR","SETCHAR");
  /**
   * Obsahuje seznam instrukci, ktere maji tri argumenty. 
   * Prvni je navesti, druhy je promenna nebo konstanta a treti je promenna nebo konstanta.
   * 
   * @var array
   */
  public static $inst_label_symb_symb = array("JUMPIFEQ","JUMPIFNEQ");


  /**
   * Funkce zkontroluje, jestli $var obsahuje text v platnem formatu pro promennou.
   * 
   * @param string $var Textova reprezentace nazvu promenne.
   * @return bool Vraci se true, pokud $var muze byt promenna.
   */
  public static function CheckVar($var = "") {
    $val = preg_match("/^(GF|TF|LF)@[a-zA-Z_%&\$\*\-][a-zA-Z0-9_%&\$\*\-]*$/", $var);
    return $val;
  }
  /**
   * Funkce zkontroluje, jestli $var obsahuje text v platnem formatu pro datovy typ.
   * 
   * @param string $var Textova reprezentace nazvu datoveho typu.
   * @return bool Vraci se true, pokud $var je platny datovy typ.
   */
  public static function CheckType($var = "") {
    $val = preg_match("/^(int|bool|string)$/", $var);
    return $val;
  }
  /**
   * Funkce zkontroluje, jestli $var obsahuje text v platnem formatu pro konstantu.
   * 
   * @param string $var Textova reprezentace nazvu konstanty.
   * @return bool Vraci se true, pokud $var muze byt platna konstanta.
   */
  public static function CheckConstant($var = "") {
    $val = preg_split( "/@/", $var,2 );
    $type = $val[0];
    $check_type = self::CheckType($type);
    if(!$check_type) { return false; }

    $check_val = false;
    $val = preg_replace( ("/^$type@/") ,'',$var);

    switch($type) {
      case "int":
        $check_val = preg_match("/^[+-]{0,1}[\d]+$/", $val); 
        break;
      case "bool":
        $check_val = preg_match("/^(true|false)$/", $val); 
        break;
      case "string":
        // Kontrola zda se v retezci nenachazi znaky: 
        // 000-032 \x00-\x20
        // 035 \x23
        // 092 \x5c
        $check_val = preg_match("/(?=^((?![\x5c\x01-\x20\x23]).)*$)/u", $val);
        // Kontrola spravnosti escape sekvence \ddd
        $tmp = preg_replace("/\\\\(\\d\\d\\d)/",'x',$val);
        $check_esc = preg_match("/(\\\\)/",$tmp);
        //error_log(">$check_val x $check_esc<\n");
        $check_val = ($check_val && (!$check_esc));

        break;
      default:
        $check_val = false;
        break;
    }

    return ( $check_type && $check_val );
  }
  /**
   * Funkce zkontroluje, jestli $var obsahuje text v platnem formatu pro navesti.
   * 
   * @param string $var Textova reprezentace nazvu navesti.
   * @return bool Vraci se true, pokud $var muze byt platne navesti.
   */
  public static function CheckLabel($var = "") {
    $val = preg_match("/^[a-zA-Z_%&\$\*\-][a-zA-Z0-9_%&\$\*\-]*$/", $var);
    return $val;
  }
  /**
   * Funkce zkontroluje, jestli $var obsahuje text v platnem formatu pro promennou nebo konstantu.
   * 
   * @param string $var Textova reprezentace nazvu promenne nebo konstanty.
   * @return bool Vraci se true, pokud $var muze byt promenna nebo konstanta.
   */
  public static function CheckSymb($var = "") {
    return ( self::CheckVar($var) || self::CheckConstant($var) );
  }

  /**
   * Funkce zjisti o jaky typ parametru se jedna a zformuje pole
   * ve formatu: index 0 = typ_dat; 1 = vlastni_data.
   * Typ_dat: var = promena, type = datovy typ, label = navesti, int = cele cislo, string = textovy retezec, bool = true/false
   * 
   * @param string $var Parametr, ktery je treba rozpoznat.
   * @return array Vraci pole ve formatu: index 0 = typ_dat; 1 = vlastni_data.
   */
  public static function FormatParamToArray($var = "") {
    if( self::CheckVar($var) ) {
      return array("var", "$var");
    }
    if( self::CheckType($var) ) {
      return array("type", "$var");
    }
    if( self::CheckConstant($var) ) {
      $parts = preg_split("/@/", $var,2);
      return array("$parts[0]", "$parts[1]");
    }
    if( self::CheckLabel($var) ) {
      return array("label", "$var");
    }
    return array();
  }

} // class


?>
