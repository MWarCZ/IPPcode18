<?php
/* -/\/\-\/\/- */

/*

<program language='ippcode18'>
  <instruction order='1' opcode='ADD'>
    <arg1 type='var'>LF@variable<\arg1>
    <arg2 type='var'>TF@__private<\arg1>
    <arg3 type='int'>1234<\arg1>
  <\instruction>
<\program>

 */

// include_once('errors.php');
require_once('errors.php');

/**
 * Slouzi ke generovani XML dokumentu pro jazyk IPPcode18.
 */
class XMLGenerator {
  /**
   * Uchovava instanci DomDocument, pomoci ktere se generuje XML dokument.
   * @var DomDocument
   */
  private $dom = null;
  /**
   * Uchovava instanci DOMElement, do ktereho jsou vkladany vsechny vygenerovane instrukce.
   * @var DOMElement
   */
  private $program = null;
  /**
   * Pocitadlo vygenerovanych instrukci.
   * @var integer
   */
  private $inst_count = 0;

  /**
   * Konstruktor
   */
  public function __construct() {
    $this->dom = new DomDocument("1.0", "UTF-8");
    $this->program = $this->dom->createElement('program');
    $this->program->setAttribute('language','IPPcode18');
    $this->dom->appendChild($this->program);
  }

  /**
   * Upravi textovy retezec, tak aby bylo mozne ho vlozit do XML dokumentu.
   * @param string $val Textovy retezec, ktery ma byt upraven.
   * @return string Vraci uprveny textovy retezec, ktery je mozne bezpecne vlozit do XML dokumentu.
   */
  private function EscapeString($val = "") {
    return htmlspecialchars($val, ENT_XML1 | ENT_COMPAT, 'UTF-8');
    //return htmlspecialchars($val, ENT_XML1 | ENT_QUOTES, 'UTF-8');
  }

  /**
   * Funkce vygeneruje a vlozi novy element instruction do XML dokumentu.
   * @param string $inst      Nazev/Kod instrukce. (pr. ADD)
   * @param string $arg1_type Typ 1.argumetnu instrukce. Pokud je null tak se nevygeneruje element arg1.
   * @param string $arg1_val  Hodnota 1.argumentu instrukce.
   * @param string $arg2_type Typ 2.argumentu instrukce. Pokud je null tak se nevygeneruje element arg2.
   * @param string $arg2_val  Hodnota 2.argumentu instrukce.
   * @param string $arg3_type Typ 3.argumentu instrukce. Pokud je null tak se nevygeneruje element arg3.
   * @param string $arg3_val  Hodnota 3.argumentu instrukce.
   */
  public function AddInstruction($inst = 'NOP', $arg1_type = null, $arg1_val = null, $arg2_type = null, $arg2_val = null, $arg3_type = null, $arg3_val = null ) {
    $inst_upper = strtoupper($inst);
    $this->inst_count++;

    $instruction = $this->dom->createElement('instruction');
    $instruction->setAttribute('order',"$this->inst_count");
    $instruction->setAttribute('opcode',"$inst_upper");

    $this->program->appendChild($instruction);

    if($arg1_type != null) {
      $arg1_val = $this->EscapeString($arg1_val);
      $arg1_type = $this->EscapeString($arg1_type);

      $arg = $this->dom->createElement('arg1', "$arg1_val");
      $arg->setAttribute('type',"$arg1_type");
      $instruction->appendChild($arg);
    }
    if($arg2_type != null) {
      $arg2_val = $this->EscapeString($arg2_val);
      $arg2_type = $this->EscapeString($arg2_type);
      
      $arg = $this->dom->createElement('arg2', "$arg2_val");
      $arg->setAttribute('type',"$arg2_type");
      $instruction->appendChild($arg);
    }
    if($arg3_type != null) {
      $arg3_val = $this->EscapeString($arg3_val);
      $arg3_type = $this->EscapeString($arg3_type);
      
      $arg = $this->dom->createElement('arg3', "$arg3_val");
      $arg->setAttribute('type',"$arg3_type");
      $instruction->appendChild($arg);
    }
  }

  /**
   * Vytiskne na standartni textovy vystup aktualni XML dokument.
   * @param boolean $formatOutput Urcuje zda bude vytisknut krasne formatovany vystup (true) nebo jednoradkovy zhusteny vypis (false).
   */
  public function PrintXML($formatOutput = false) {
    $this->dom->formatOutput = $formatOutput;
    $str = $this->dom->saveXML();
    echo "$str";
  }

}


?>
