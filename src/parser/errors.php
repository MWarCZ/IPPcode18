<?php
/* -/\/\-\/\/- */

/**
 * Trida obsahuje konstanty chyb, ktere mohou nastat.
 */
abstract class Errors {
  /**
   * Chybejici parametr skriptu nebo chybna kombinace.
   */
  const ERR_INPUT_PARAM = 10;
  /**
   * Chyba pri otevirani vstupniho souboru.
   */
  const ERR_INPUT_FILE = 11;
  /**
   * Chyba pri otevirani vystupniho souboru pro zapis.
   */
  const ERR_OUTPUT_FILE = 12;
  /**
   * Lexikalni nebo syntakticka chyba pri zpracovavani IPPcode18
   */
  const ERR_LEX_SYN = 21;

  /**
   * Interni chyba
   */
  const ERR_INTERNAL = 99;

}

?>
