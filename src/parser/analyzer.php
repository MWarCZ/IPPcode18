<?php
/* -/\/\-\/\/- */

// include_once('errors.php');
require_once('errors.php');

// include_once('checker.php');
require_once('checker.php');


class Analyzer {

  /**
   * Pocet všech nactenych radku.
   * @var integer
   */
  public $lines_count = 0;
  /**
   * Pocet radku na kterych byl komentar.
   * @var integer
   */
  public $comments_count = 0;
  /**
   * Pocet radku na kterych byl uzitecny kod.
   * @var integer
   */
  public $commands_count = 0;

  /**
   * Ukazuje zda byl jiz precten cely vstup.
   * @var boolean
   */
  public $feof = false;

  /**
   * Vstupni stream. Jde o ukazatel na soubor ze ktereho se budou cist radky.
   * @var resource
   */
  private $stream = STDIN;


  /**
   * Konstruktor.
   * @param resource $stream Ukazatel na soubor ze ktereho se bude cist. Pokud neni zadan, nastavi se na STDIN.
   */
  function __construct($stream = STDIN) {
    $this->stream = $stream;
  }

  /**
   * Funkce cte radek ze vstupniho streamu dat.
   * @return string Vraci precteny radek.
   */
  private function ReadLine() {
      if( !feof($this->stream) ) {
        $line = fgets($this->stream);
        $this->feof = feof($this->stream);
        $this->lines_count++;
        return $line;
      }
      return "";
  }

  /**
   * Funkce precte radek a otestuje zda obsahuje ".ippcode18".
   * @return bool Obsahuje precteny radek ".ippcode18"?
   */
  public function CheckHeader(&$err = 0) {

    if( !($this->feof) ) {
      // Nacte radek vstupu
      $line = $this->ReadLine();

      $val = preg_split( "/#/", $line );
      $line = $val[0];
      // Pokud je pole vedsi nez 1, tak byl na radku komentar.
      if( count($val)>1 ) { $this->comments_count++; }

      $line = strtoupper($line);
      $line = trim($line);

      $val = preg_match("/^\.IPPCODE18$/i", $line);
      if( !$val ) {
        error_log("Program obsahuje chybnou hlavičku. Je ocekavana hlavicka '.ippcode18'.\n");
        $err = Errors::ERR_LEX_SYN;
      }
      return $val;
    }
    $err = Errors::ERR_LEX_SYN;
    return false;
  }

  /**
   * Funkce precte jeden radek a zkontroluje jeho lexikalni a
   * syntaktickou spravnost.
   * @param integer &$err Zde bude ulozen kod chyby, pokud nastane.
   * @return array Vraci pole stringu a poli. Na indexu 0 je instrukce a na dalsich indexech jsou parametry. Funkce muze vratit NULL nebo prazdne pole, proto je nutne zkontrolovat &$err.
   */
  public function GetTokens(&$err = 0) {
    // Poke ktere funkce fude vracet
    $output = array();

    if( !($this->feof) ){
    
      // Nacte radek vstupu
      $line = $this->ReadLine();

      $val = preg_split( "/#/", $line );
      // Pokud je pole vedsi nez 1, tak byl na radku komentar.
      if( count($val)>1 ) { $this->comments_count++; }
      // Odstraneni duplicitnich bilich znaku
      $val = preg_replace("/[ \t]+/", " ", $val);
      // Odstrani nadbitecne mezery od zacatku a od konce.
      $line = trim( $val[0] );

      // Rozdeleni prikazu na jednotlive casti
      $parts = preg_split("/ /", $line);
      $instruction = strtoupper($parts[0]);
      $inst_argc = count($parts)-1;

      $this->commands_count++;

      
      if( $instruction == "" ) {
        $this->commands_count--;
      }
      elseif( in_array($instruction, Checker::$inst) ) {
        if($inst_argc != 0) { 
          error_log("$this->lines_count: Instrukce '$instruction' nema mit zadne parametry.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
      }
      else if( in_array($instruction, Checker::$inst_var) ) {
        if($inst_argc != 1) {
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 1 parametr.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckVar($parts[1]) ) {
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format promenne.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
      }
      else if( in_array($instruction, Checker::$inst_symb) ) {
        if($inst_argc != 1) { 
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 1 parametr.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckSymb($parts[1]) ) { 
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format promenne nebo konstanty.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
        
      }
      else if( in_array($instruction, Checker::$inst_label) ) {
        if($inst_argc != 1) { 
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 1 parametr.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckLabel($parts[1]) ) { 
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format navesti.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
        
      }
      else if( in_array($instruction, Checker::$inst_var_symb) ) {
        if($inst_argc != 2) {
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 2 parametry.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckVar($parts[1]) ) { 
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format promenne.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckSymb($parts[2]) ) { 
          error_log("$this->lines_count: 2. parametr instrukce '$instruction' neni platny format promenne nebo konstanty.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
        array_push($output, Checker::FormatParamToArray($parts[2]) );
        
      }
      else if( in_array($instruction, Checker::$inst_var_type) ) {
        if($inst_argc != 2) { 
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 2 parametry.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckVar($parts[1]) ) {  
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format promenne.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckType($parts[2]) ) { 
          error_log("$this->lines_count: 2. parametr instrukce '$instruction' neni platny format datoveho typu.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
        array_push($output, Checker::FormatParamToArray($parts[2]) );
        
      }
      else if( in_array($instruction, Checker::$inst_var_symb_symb) ) {
        if($inst_argc != 3) { 
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 3 parametry.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckVar($parts[1]) ) {  
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format promenne.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckSymb($parts[2]) ) { 
          error_log("$this->lines_count: 2. parametr instrukce '$instruction' neni platny format promenne nebo konstanty.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckSymb($parts[3]) ) { 
          error_log("$this->lines_count: 3. parametr instrukce '$instruction' neni platny format promenne nebo konstanty.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
        array_push($output, Checker::FormatParamToArray($parts[2]) );
        array_push($output, Checker::FormatParamToArray($parts[3]) );
        
      }
      else if( in_array($instruction, Checker::$inst_label_symb_symb) ) {
        if($inst_argc != 3) { 
          error_log("$this->lines_count: Instrukce '$instruction' ma mit 3 parametry.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckLabel($parts[1]) ) {  
          error_log("$this->lines_count: 1. parametr instrukce '$instruction' neni platny format navesti.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckSymb($parts[2]) ) {  
          error_log("$this->lines_count: 2. parametr instrukce '$instruction' neni platny format promenne nebo konstanty.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }
        if( !Checker::CheckSymb($parts[3]) ) {  
          error_log("$this->lines_count: 3. parametr instrukce '$instruction' neni platny format promenne nebo konstanty.\n"); 
          $err=Errors::ERR_LEX_SYN; return null; }

        // Naplneni vystupniho pole
        array_push($output, "$instruction");
        array_push($output, Checker::FormatParamToArray($parts[1]) );
        array_push($output, Checker::FormatParamToArray($parts[2]) );
        array_push($output, Checker::FormatParamToArray($parts[3]) );
        
      }
      else {
        $this->commands_count--;
        error_log("$this->lines_count: Neznami prikaz.\n"); 
        $err=Errors::ERR_LEX_SYN; return null;
      }

      //return $parts;
      return $output;
    }
  }


} // class


?>
