
import sys
from pathlib import Path

from errors import Errors
from options import OptParser
from interpret import Interpret
from xmlloader import XMLLoader


class App(object):
	def __init__(self, argv):
		self.parser = OptParser(argv[1:])
		self.interpret = Interpret()

	def Run(self):
		(options, args) = self.parser.ParseArgs()
		print('options:', options)
		run = ''
		try:
			if options.help:
				if len(self.parser.argv)==1 :
					run = 'help'
					self.parser.PrintHelp()
			elif options.source:
				my_file = Path(options.source)
				if my_file.is_file():
					print('source ok: ', options.source)
					run = 'program'
				else:
					msg = "Zdrojovy soubor neexistuje."
					Errors.Exit(Errors.INPUT_FILE, msg)

			else:
				Errors.Exit(Errors.INPUT_PARAM, 'Chybna kombinace prepinacu.')

			if options.stats and (options.insts or options.vars):
				print('stats ok')
			elif options.stats or options.insts or options.vars:
				Errors.Exit(Errors.INPUT_PARAM, 'Chybna kombinace prepinacu.')

			if run=='help':
				self.parser.PrintHelp()
			elif run=='program':
				interpret = Interpret()
				xml = XMLLoader()
				xml.LoadProgram(interpret, options.source)
				interpret.RunProgram()
		except ValueError as err:
			print(err.args[1], file=sys.stderr)
			sys.exit(err.args[0])
		...

app = App(sys.argv)
app.Run()
