
import sys
from errors import Errors

from interpret import Interpret
import collections

import xml.etree.ElementTree as etree


class LexSynAnal(object):
	@staticmethod
	def CheckTagName(xml_node, tag_name, msg=None):
		msg = "Nejedna se o tag '%s'"%(tag_name) if msg==None else msg
		if xml_node.tag != tag_name:
			Errors.Exit(Errors.LEX_SYN, msg)

	@staticmethod
	def CheckTagAtrib(xml_node, must_atrib = [], optional_atrib = [], msg=None):
		must_have = { value: 0 for value in must_atrib }
		keys = xml_node.keys()

		for key in keys:
			if key in must_atrib:
				must_have[key]+=1
			elif not (key in optional_atrib):
				msg = "Tag '%s' obsahuje nepovoleny atribut '%s'."%(xml_node.tag, key) if msg==None else msg
				Errors.Exit(Errors.LEX_SYN, msg)

		for key, value in must_have.items():
			if value != 1 :
				msg = "Tag '%s' neobsahuje povinny atribut '%s'."%(xml_node.tag, key) if msg==None else msg
				Errors.Exit(Errors.LEX_SYN, msg)
				
	@staticmethod
	def GetValueOfType( value, text_datatype, msg=None):
		res = None
		text_datatype = text_datatype.lower()
		try:
			if text_datatype=='int':
				res = int(value)
			elif text_datatype=='bool':
				if data.lower()=='true':
					res = True
				elif data.lower()=='false':
					res = False
				else:
					raise ValueError('bool')
			elif text_datatype=='string' or text_datatype=='var' or text_datatype=='label':
				if type(value)==type('str'):
					res = value
				else:
					raise ValueError('string')
			elif text_datatype=='type':
				if type(value)==type('str'):
					if value=='bool':
						res=type(True)
					elif value=='int':
						res=type(1)
					elif value=='string':
						res=type('sss')
					elif value=='float':
						res=type(1.2)
					else:
						res = type(None)
				else:
					raise ValueError('string')

			elif text_datatype=='float':
				res = float(value)
			else:
				raise ValueError('Unknown')

			return res
		except:
			msg = "Hodnota neni ocekavaneho typu." if msg==None else msg
			Errors.Exit(Errors.LEX_SYN, msg)

	@staticmethod
	def CheckType( value, datatype, msg=None):
		res = None
		try:
			if datatype==type(1):
				res = int(value)
			elif datatype==type(True):
				if data.upper()=='TRUE':
					res = True
				elif data.upper()=='FALSE':
					res = False
				else:
					raise ValueError('bool')
			elif datatype==type('str'):
				if type(value)==type('str'):
					res = value
				else:
					raise ValueError('string')
			elif datatype==type(1.1):
				res = float(value)
			else:
				raise ValueError('Unknown')
			return res
		except:
	 		msg = "Hodnota neni ocekavaneho typu." if msg==None else msg
	 		Errors.Exit(Errors.LEX_SYN, msg)

	# 

	@staticmethod
	def Program( xml_node ):
		LexSynAnal.CheckTagName(xml_node, 'program')
		LexSynAnal.CheckTagAtrib(xml_node,['language'], ['name','description'] )
		language = LexSynAnal.CheckType(xml_node.attrib['language'],type('str'))
		return [language]

	@staticmethod
	def Instruction( xml_node ):
		LexSynAnal.CheckTagName(xml_node, 'instruction')
		LexSynAnal.CheckTagAtrib(xml_node,['order','opcode'], [] )
		order = LexSynAnal.CheckType(xml_node.attrib['order'],type(1))
		opcode = LexSynAnal.CheckType(xml_node.attrib['opcode'],type('str'))
		return [order, opcode]

	@staticmethod
	def Arg( xml_node, needer, haver, instruction_args ):
		LexSynAnal.CheckTagAtrib(xml_node,['type'], [] )
		typ = xml_node.attrib['type'].lower()

		if xml_node.tag=='arg1':
			if not needer[0]:
				msg = "Tag '%s' nebyl ocekavan."%(xml_node.tag)
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - nebyl ocekavan
			if haver[0]!=None:
				msg = "Tag '%s' je duplicitni."%(xml_node.tag)
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - duplicita
			if not (typ in instruction_args[1]):
				msg = "Instrukce obsahuje parametry nespravneho datoveho typu."
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - necekany typ
			haver[0] = [ LexSynAnal.GetValueOfType(xml_node.text, typ), typ ]

		elif xml_node.tag=='arg2':
			if not needer[1]:
				msg = "Tag '%s' nebyl ocekavan."%(xml_node.tag)
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - nebyl ocekavan
			if haver[1]!=None:
				msg = "Tag '%s' je duplicitni."%(xml_node.tag)
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - duplicita
			if not (typ in instruction_args[2]):
				msg = "Instrukce obsahuje parametry nespravneho datoveho typu."
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - necekany typ
			haver[1] = [ LexSynAnal.GetValueOfType(xml_node.text, typ), typ ]

		elif xml_node.tag=='arg3':
			if not needer[2]:
				msg = "Tag '%s' nebyl ocekavan."%(xml_node.tag)
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - nebyl ocekavan
			if haver[2]!=None:
				msg = "Tag '%s' je duplicitni."%(xml_node.tag)
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - duplicita
			if not (typ in instruction_args[3]):
				msg = "Instrukce obsahuje parametry nespravneho datoveho typu."
				Errors.Exit(Errors.LEX_SYN, msg)
				... # err - necekany typ
			haver[2] = [ LexSynAnal.GetValueOfType(xml_node.text, typ), typ ]

		else:
			msg = "Tag nebyl ocekavan."
			Errors.Exit(Errors.LEX_SYN, msg)


	TABLE_OPCODE = {
		#
		'MOVE':[
			[True, True, False],
			['var'],
			['var','int','string','bool'],
			[] 
		], 
		'CREATEFRAME':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'PUSHFRAME':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'POPFRAME':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'DEFVAR':[
			[True, False, False],
			['var'],
			[],
			[] 
		],
		'CALL':[
			[True, False, False],
			['label'],
			[],
			[] 
		],
		'RETURN':[
			[False, False, False],
			[],
			[],
			[] 
		],
		#
		'PUSHS':[
			[True, False, False],
			['var','int','string','bool'],
			[],
			[]
		], 
		'POPS':[
			[True, False, False],
			['var'],
			[],
			[] 
		],
		'CLEARS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		#
		'ADD':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','int']
		], 
		'ADDS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'SUB':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','int']
		], 
		'SUBS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'MUL':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','int']
		], 
		'MULS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'IDIV':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','int']
		], 
		'IDIVS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		#
		'LT':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','int']
		], 
		'LTS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'GT':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','int']
		], 
		'GTS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'EG':[
			[True, True, True],
			['var'],
			['var','int','bool'],
			['var','int','bool']
		], 
		'EGS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'AND':[
			[True, True, True],
			['var'],
			['var','bool'],
			['var','bool']
		], 
		'ANDS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'OR':[
			[True, True, True],
			['var'],
			['var','bool'],
			['var','bool']
		], 
		'ORS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'NOT':[
			[True, True, False],
			['var'],
			['var','bool'],
			[]
		], 
		'NOTS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		#
		'INT2CHAR':[
			[True, True, False],
			['var'],
			['var','int'],
			[]
		], 
		'INT2CHARS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'STRI2INT':[
			[True, True, True],
			['var'],
			['var','string'],
			['var','int']
		], 
		'STRI2INTS':[
			[False, False, False],
			[],
			[],
			[] 
		],
		'READ':[
			[True, True, False],
			['var'],
			['type'],
			[] 
		],
		'WRITE':[
			[True, False, False],
			['var','int','bool','string'],
			[],
			[] 
		],
		#
		'CONCAT':[
			[True, True, True],
			['var'],
			['var','string'],
			['var','string']
		], 
		'STRLEN':[
			[True, True, False],
			['var'],
			['var','string'],
			[]
		], 
		'GETCHAR':[
			[True, True, True],
			['var'],
			['var','string'],
			['var','int']
		], 
		'SETCHAR':[
			[True, True, True],
			['var'],
			['var','int'],
			['var','string']
		], 
		'TYPE':[
			[True, True, False],
			['var'],
			['var','int','string','bool'],
			[]
		],
		#
		'LABEL':[
			[True, False, False],
			['label'],
			[],
			[]
		],
		'JUMP':[
			[True, False, False],
			['label'],
			[],
			[]
		],
		'JUMPIFEQ':[
			[True, True, True],
			['label'],
			['var','int','string','bool'],
			['var','int','string','bool']
		],
		'JUMPIFEQS':[
			[True, False, False],
			['label'],
			[],
			[]
		],
		'JUMPIFNEQ':[
			[True, True, True],
			['label'],
			['var','int','string','bool'],
			['var','int','string','bool']
		],
		'JUMPIFNEQS':[
			[True, False, False],
			['label'],
			[],
			[]
		],
		#
		'DPRINT':[
			[False, False, False],
			[],
			[],
			[]
		],
		'BREAK':[
			[False, False, False],
			[],
			[],
			[]
		]
		#	
	}


	@staticmethod
	def CheckOpcode(opcode):
		try:
			opcode = opcode.upper()
			return LexSynAnal.TABLE_OPCODE[opcode]
		except:
			msg = "Dany kod instrukce nebyl nalezen."
			Errors.Exit(Errors.LEX_SYN, msg)

#
#
class XMLLoader(object):

	def LoadProgram(self, interpret, filename):
		try:
			with open(filename, 'r') as xml_file:
				xml_tree = etree.parse(xml_file)
			...
		except:
			msg = "Nepodarilo se otevrit zdrojovy soubor."
			Errors.Exit(Errors.WELL_FORMATED, msg)
			... # err read from file

		tmp_program = {}
		
		# <program ...>
		tag_program = xml_tree.getroot()
		LexSynAnal.Program(tag_program)
		# <instruction ...>
		tags_instruction = tag_program.getchildren()
		for tag_instruction in tags_instruction:
			order, opcode = LexSynAnal.Instruction(tag_instruction)

			# <arg? ...>
			instruction_args = LexSynAnal.CheckOpcode(opcode)
			
			needer = instruction_args[0]
			haver = [None, None, None]

			tags_arg = tag_instruction.getchildren()

			for tag_arg in tags_arg:
				LexSynAnal.Arg(tag_arg, needer, haver, instruction_args )
			xxx = [haver[0]!=None, haver[1]!=None, haver[2]!=None]
			if needer!=xxx:
				msg = "Instrukce obsahuje nespravny pocet parametru."
				Errors.Exit(Errors.LEX_SYN, msg)
			#
			if order in tmp_program.keys():
				msg = "Nektere instrukce se maji vykonat soucasne - to neni mozne."
				Errors.Exit(Errors.LEX_SYN, msg)
				... #err
			tmp_program[order] = [opcode, haver ]

		tmp_indexs = collections.OrderedDict(sorted(tmp_program.items()))

		for index in tmp_indexs:
			#print(' XXX ',tmp_program[index],' XXX')
			self.SetCommand(interpret, tmp_program[index][0],tmp_program[index][1])
			...
	def SetCommand(self, interpret, opcode, params):
		TABLE_OPCODE = {		
			'MOVE':interpret.Move,
			'CREATEFRAME': interpret.Createframe,
			'PUSHFRAME': interpret.Pushframe,
			'POPFRAME': interpret.Popframe,
			'DEFVAR': interpret.Defvar,
			'CALL': interpret.Call,
			'RETURN': interpret.Return,
			'PUSHS': interpret.Pushs,
			'POPS': interpret.Pops,
			'CLEARS': interpret.Clears,
			'ADD': interpret.Add,
			'ADDS': interpret.Adds,
			'SUB': interpret.Sub,
			'SUBS': interpret.Subs,
			'MUL': interpret.Mul,
			'MULS': interpret.Muls,
			'IDIV': interpret.Idiv,
			'IDIVS': interpret.Idivs,
			'LT': interpret.Lt,
			'LTS': interpret.Lts,
			'GT': interpret.Gt,
			'GTS': interpret.Gts,
			'EG': interpret.Eq,
			'EGS': interpret.Eqs,
			'AND': interpret.And,
			'ANDS': interpret.Ands,
			'OR': interpret.Or,
			'ORS': interpret.Ors,
			'NOT': interpret.Not,
			'NOTS': interpret.Nots,
			'INT2CHAR': interpret.Int2char,
			'INT2CHARS': interpret.Int2chars,
			'STRI2INT': interpret.Stri2int,
			'STRI2INTS': interpret.Stri2ints,
			'READ': interpret.Read,
			'WRITE': interpret.Write,
			'CONCAT': interpret.Concat,
			'STRLEN': interpret.Strlen,
			'GETCHAR': interpret.Getchar,
			'SETCHAR': interpret.Setchar,
			'TYPE': interpret.Type,
			'LABEL': interpret.Label,
			'JUMP': interpret.Jump,
			'JUMPIFEQ': interpret.Jumpifeq,
			'JUMPIFEQS': interpret.Jumpifeqs,
			'JUMPIFNEQ': interpret.Jumpifneq,
			'JUMPIFNEQS': interpret.Jumpifneqs,
			'DPRINT': interpret.Dprint,
			'BREAK': interpret.Break
		}
		nums=0
		tmp_params = []
		tmp_isvar = []
		for param in params:
			if param!=None:
				nums+=1
				tmp_params+=[param[0]]
				tmp_isvar+=[param[1]=='var']
		tmp_params += [tmp_isvar]

		if opcode=='LABEL':
			interpret.AddLabel( tmp_params[0] )
		elif opcode=='READ':
			interpret.AddCommand( TABLE_OPCODE[opcode], tmp_params )
		else:
			if nums==0:
				interpret.AddCommand( TABLE_OPCODE[opcode] )
				...
			elif nums>=1:
				interpret.AddCommand( TABLE_OPCODE[opcode], tmp_params )
				...
		...
	# interpret.AddCommand( interpret.Write, ('mezi') )
	# interpret.AddLabel( 'fun' )

#
#

# interpret = Interpret()
# xml = XMLLoader()
# xml.LoadProgram(interpret,'bbb.xml')

# interpret.RunProgram()


