
# import sys

class Errors(object):
	"""Trida s chybovymi kody, ktere muze interpret vracet."""
	INPUT_PARAM = 10
	INPUT_FILE = 11
	OUTPUT_FILE = 12
	WELL_FORMATED = 31
	LEX_SYN = 32

	SEM = 52
	RUN_INCOMP_TYPE = 53
	RUN_UNEXIST_VAR = 54
	RUN_UNEXIST_FRAME = 55
	RUN_MISS_VALUE = 56
	RUN_DIV_BY_ZERO = 57
	RUN_STRING = 58

	INTERNAL = 99

	@staticmethod
	def Exit( code, msg=None):
		msg = 'Doslo k chybe.' if msg==None else msg
		# print(msg, file=sys.stderr)
		# sys.exit(code)
		raise ValueError(code, msg)

	@staticmethod
	def ExitOn(code, num, msg=None):
		msg = 'Doslo k chybe.' if msg==None else msg
		msg = str(num)+': '+ msg
		Errors.Exit(code, msg )

