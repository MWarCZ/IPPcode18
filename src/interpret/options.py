
import sys
from errors import Errors

# --help
# --source=file
# --stats=FILE --insts --vars

from optparse import OptionParser

class OptionParser2(OptionParser):
    def error(self, message):
        self.print_help(sys.stderr)
        print('%s: Error: %s\n' % (self.prog, message), file=sys.stderr)
        self.exit( Errors.INPUT_PARAM )

class OptParser(object):
	def __init__(self, argv):
		self.argv = argv

		usage = 'usage: %prog [options]'
		self.parser = OptionParser2(usage=usage, add_help_option=False)

		self.parser.add_option('-h', '--help', action="store_true", help='Vypise tuto napovedu.')
		self.parser.add_option('-s', '--source', action='store', type='string', dest='source', metavar='FILE', help='Cesta k xml souboru IPPcode18.')

		self.parser.add_option('--stats', action='store', type='string', dest='stats', metavar='FILE', help='Budou se zapisovat statistiky do souboru.')
		self.parser.add_option('--insts', action="count", help='Do statistik se zapise pocet vykonanych instrukci. Musi se kombinovat s --stats.')
		self.parser.add_option('--vars', action="count", help='Do statistik se zapise pocet vsech inicializovanych promennych. Musi se kombinovat s --stats.')

	def PrintHelp(self):
		print(self.parser.format_help().strip() )

	def ParseArgs(self):
		return self.parser.parse_args(self.argv)

	def GetStatsFormat(self):	
		stats_format = ''
		for x in self.argv:
			if x=='--insts':
				stats_format += "I\n"
			elif x=='--vars':
				stats_format += "V\n"
		return stats_format




# parser = OptParser(sys.argv[1:])
# (options, args) = parser.ParseArgs()
# print('args: ', args)
# print('options: ', options)
# parser.PrintHelp()

# print(parser.GetStatsFormat())
