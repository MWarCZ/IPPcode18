
from errors import Errors
from structures import Stack, Frame
from function import function

class Interpret(object):
	def __init__(self):
		self.StackLF = Stack()
		self.GF = Frame()
		self.LF = None
		self.TF = None
		self.program = []
		self.programCounter = 0
		self.labels = {}
		self.StackCall = Stack()
		self.StackData = Stack()

		self.instCounter = 0
	#
	def ParseFrameVar(self, fullvar):
		return fullvar.split('@',1)

	def GetFrame(self, frame='GF'):
		if frame == 'GF':
			return self.GF
		elif frame == 'LF':
			return self.LF
		elif frame == 'TF':
			return self.TF
		else:
			Errors.ExitOn(Errors.INTERNAL, self.programCounter, 'Neznami nazev ramec. Znami jsou nazvy GF, LF, TF.')

	def ExistVar(self, fullvar): 
		frame,var = self.ParseFrameVar(fullvar)
		frame = self.GetFrame(frame)
		if frame==None:
			Errors.ExitOn(Errors.RUN_UNEXIST_FRAME, self.programCounter, 'Pristup k neexistujicimu ramci.')
		if not frame.ExistVar(var):
			Errors.ExitOn(Errors.RUN_UNEXIST_VAR, self.programCounter, 'Pristup k neexistujici promenne.')
		return [frame, var]

	def GetValueSymb(self, symb, var_only=False, not_test=False):
		if var_only:
			frame,var = self.ExistVar(symb)
			data = frame.GetVar(var)
			if (not not_test) and data==None:
				Errors.ExitOn(Errors.RUN_MISS_VALUE, self.programCounter, 'Chybejici hodnota promenne.')
		else:
			data = symb
		return data

	def TestSameType(self, var1, var2, can_type=None):
		res = type(var1) == type(var2)
		if can_type!=None:
			res = res and (type(var1) in can_type)
		if not res:
			Errors.ExitOn(Errors.RUN_INCOMP_TYPE, self.programCounter, 'Spatne typy operandu.')
		return res

	def PopStackData(self):
		data = self.StackData.Pop()
		if data==None:
			Errors.ExitOn(Errors.RUN_MISS_VALUE, self.programCounter, 'Chybejici hodnota na datovem zasobniku.')
		return data

	#
	def AddCommand(self, func, params=None):
		f = function( func, params )
		self.program.append(f);

	def AddLabel(self, label):
		if label in self.labels.keys():
			Errors.ExitOn(Errors.SEM, self.programCounter, 'Pokus o redefinici existujiciho navesti.')

		index = len(self.program)
		self.AddCommand(self.Label, index)
		self.labels[label]=index

	def RunProgram(self):
		self.programCounter = 0
		while( self.programCounter < len(self.program) ):
			self.program[self.programCounter].call()
			self.programCounter += 1
	#
	#
	def Move(self, fullvar, symb, var_only=[True, False]):
		frame,var = self.ExistVar(fullvar)
		data = self.GetValueSymb(symb, var_only[1])
		frame.SetVar(var, data)

	def Createframe(self):
		self.TF = Frame()

	def Pushframe(self):
		self.StackLF.Push(self.TF)
		self.LF = self.StackLF.Top();
		self.TF = None

	def Popframe(self):
		self.TF = self.StackLF.Pop()
		if self.TF==None:
			Errors.ExitOn(Errors.RUN_UNEXIST_FRAME, self.programCounter, 'Ramec neexistuje. Neexistuje zadny lokalni ramec.')

	def Defvar(self, fullvar, var_only=[True]):
		frame,var = self.ParseFrameVar(fullvar)
		frame = self.GetFrame(frame)
		if frame==None:
			Errors.ExitOn(Errors.RUN_UNEXIST_FRAME, self.programCounter, 'Pristup k neexistujicimu ramci.')
		if frame.ExistVar(var):
			Errors.ExitOn(Errors.RUN_UNEXIST_VAR, self.programCounter, 'Duplicitni jmeno promene v ramci.')
		frame.SetVar(var, None)

	def Call(self, label, var_only=[False]):
		if not label in self.labels.keys():
			Errors.ExitOn(Errors.SEM, self.programCounter, 'Label neexistuje.')
		self.StackCall.Push(self.programCounter)
		self.programCounter = self.labels[label]

	def Return(self):
		ret = self.StackCall.Pop()
		if ret==None:
			Errors.ExitOn(Errors.RUN_MISS_VALUE, self.programCounter, 'Neni zadna adresa na kterou se ma vracet.')
		self.programCounter = ret

	#
	def Pushs(self, symb, var_only=[False]):
		data = self.GetValueSymb(symb, var_only[0])
		self.StackData.Push(data)
		
	def Pops(self, fullvar, var_only=[True]):
		frame,var = self.ExistVar(fullvar)
		data = self.PopStackData()
		frame.SetVar(var, data)

	def Clears(self):
		self.StackData = Stack()

	#
	def Add(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 + data2)

	def Adds(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()

		self.TestSameType(data1, data2, [type(1)])
		self.StackData.Push(data1 + data2)

	def Sub(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 - data2)

	def Subs(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1)])
		self.StackData.Push(data1 - data2)

	def Mul(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 * data2)

	def Muls(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1)])
		self.StackData.Push(data1 * data2)

	def Idiv(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1)])
		if data2==0:
			Errors.ExitOn(Errors.RUN_DIV_BY_ZERO, self.programCounter, 'Deleni nulou.')

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 // data2)

	def Idivs(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1)])
		if data2==0:
			Errors.ExitOn(Errors.RUN_DIV_BY_ZERO, self.programCounter, 'Deleni nulou.')

		self.StackData.Push(data1 // data2)

	def Div(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1.2)])
		if data2==0.0:
			Errors.ExitOn(Errors.RUN_DIV_BY_ZERO, self.programCounter, 'Deleni nulou.')

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 / data2)

	def Divs(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1.2)])
		if data2==0.0:
			Errors.ExitOn(Errors.RUN_DIV_BY_ZERO, self.programCounter, 'Deleni nulou.')

		self.StackData.Push(data1 / data2)

	#
	def Lt(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 < data2)

	def Lts(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1)])
		self.StackData.Push(data1 < data2)

	def Gt(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 > data2)

	def Gts(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1)])
		self.StackData.Push(data1 > data2)

	def Eq(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(1), type(True)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 == data2)

	def Eqs(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(1), type(True)])
		self.StackData.Push(data1 == data2)

	def And(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(True)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 and data2)

	def Ands(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(True)])
		self.StackData.Push(data1 and data2)

	def Or(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type(True)])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 or data2)

	def Ors(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, data2, [type(True)])
		self.StackData.Push(data1 or data2)

	def Not(self, fullvar, symb, var_only=[True, False]):
		data = self.GetValueSymb(symb, var_only[1])

		self.TestSameType(data1, True)

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, not data)

	def Nots(self):
		data = self.PopStackData()
		
		self.TestSameType(data, True)
		self.StackData.Push(not data)

	#
	def Int2char(self, fullvar, symb, var_only=[True, False]):
		data = self.GetValueSymb(symb, var_only[1])
		frame,var = self.ExistVar(fullvar)

		self.TestSameType(data, 1)
		# TODO - kontrola rozsahu
		data = chr(data)
		frame.SetVar(var, data)

	def Int2chars(self):
		data = self.PopStackData()
		self.TestSameType(data, 1)
		# TODO - kontrola rozsahu
		data = chr(data)
		self.StackData.Push( data )

	def Stri2int(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, 'str')
		self.TestSameType(data2, 1)

		frame,var = self.ExistVar(fullvar)
		data = data1[data2]
		frame.SetVar(var, data)

	def Stri2ints(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()
		
		self.TestSameType(data1, 'str')
		self.TestSameType(data2, 1)

		data = data1[data2]
		self.StackData.Push(data)

	# Načte jednu hodnotu dle zadaného typu <type> ∈ {int, string, bool} a uloží tuto hodnotu do proměnné <var>. Načtení proveďte vestavěnou funkcí input() jazyka Python 3, pak proveďte konverzi na specifikovaný typ <type>. Při převodu vstupu na typ bool nezáleží na velikosti písmen a řetězec „true" se převádí na bool@true, vše ostatní na bool@false. V případě chybného vstupu bude do proměnné <var> uložena implicitní hodnota (dle typu 0, prázdný řetězec nebo false).
	def Read(self, fullvar, datatype, var_only=[True, False]):
		frame, var = self.ExistVar(fullvar)
		if datatype==type(1):
			try:
				data = int(input('i: '))
			except:
				data = 0
		elif datatype==type(True):
			try:
				data = input('b: ')
				data = True if data.upper()=='TRUE' else False
			except:
				data = False
		elif datatype==type('str'):
			try:
				data = input('s: ') 
			except:
				data = ''
		elif datatype==type(1.2):
			try:
				data = float(input('f: ')) 
			except:
				data = 0.0
		else:
			Errors.ExitOn(Errors.INTERNAL, self.programCounter, 'Chcete nacist neznami datovy typ.')

		frame.SetVar(var, data)

	def Write(self, symb, var_only=[False]):
		data = self.GetValueSymb(symb, var_only[0])
		if data==False:
			data='false'
		elif data==True:
			data='true'
		print(data)
		#print(symb, end='', flush=True)
	#
	def Concat(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, data2, [type('str')])

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1 + data2)

	def Strlen(self, fullvar, symb, var_only=[True, False]):
		data1 = self.GetValueSymb(symb, var_only[1])

		self.TestSameType(data1, 'str')
		data = len(data1)

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data)

	def Getchar(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data1, 'str')
		self.TestSameType(data2, 1)

		if data2>=len(data1) or data2<0:
			Errors.ExitOn(Errors.RUN_STRING, self.programCounter, 'Index jede mimo textovy retezec.')

		frame,var = self.ExistVar(fullvar)
		frame.SetVar(var, data1[data2])

	def Setchar(self, fullvar, symb1, symb2, var_only=[True, False, False]):
		frame,var = self.ExistVar(fullvar)
		data = frame.GetVar(var)
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		self.TestSameType(data, 'str')
		self.TestSameType(data1, 1)
		self.TestSameType(data2, 'str')

		if data1>=len(data) or data1<0 or len(data2)<1:
			Errors.ExitOn(Errors.RUN_STRING, self.programCounter, 'Index jede mimo textovy retezec.')

		data = list(data)
		data[data1] = data2[0]
		data = ''.join(data) 
		frame.SetVar(var, data)

	def Type(self, fullvar, symb, var_only=[True, False]):
		frame, var = self.ExistVar(fullvar)
		datatype = None
		strtype = ''
		data = self.GetValueSymb(symb, var_only[1], not_test=True)
		datatype = type(data)

		if datatype==type(None):
			strtype = ''
		elif datatype==type(1):
			strtype = 'int'
		elif datatype==type('str'):
			strtype = 'string'
		elif datatype==type(True):
			strtype = 'bool'
		elif datatype==type(1.2):
			strtype = 'float'
		else:
			Errors.ExitOn(Errors.INTERNAL, self.programCounter, 'Neznami datovy typ.')

		frame.SetVar(var, strtype)
	#
	def Label(self, *other):
		# TODO - jeste domyslet - zatim nedelat nic
		...

	def Jump(self, label, var_only=[False]):
		if not label in self.labels.keys():
			Errors.ExitOn(Errors.SEM, self.programCounter, 'Label neexistuje.')
		self.programCounter = self.labels[label]

	def Jumpifeq(self, label, symb1, symb2, var_only=[False, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		if not label in self.labels.keys():
			Errors.ExitOn(Errors.SEM, self.programCounter, 'Label neexistuje.')

		self.TestSameType(data1, data2, [type(1), type(True)])

		if data1 == data2:
			self.programCounter = self.labels[label]
		
	def Jumpifeqs(self, label, var_only=[False]):
		data1 = self.PopStackData()
		data2 = self.PopStackData()

		self.Jumpifeq(label, data1, data2)

	def Jumpifneq(self, label, symb1, symb2, var_only=[False, False, False]):
		data1 = self.GetValueSymb(symb1, var_only[1])
		data2 = self.GetValueSymb(symb2, var_only[2])

		if not label in self.labels.keys():
			Errors.ExitOn(Errors.SEM, self.programCounter, 'Label neexistuje.')

		self.TestSameType(data1, data2, [type(1), type(True)])

		if data1 != data2:
			self.programCounter = self.labels[label]
		
	def Jumpifneqs(self):
		data1 = self.PopStackData()
		data2 = self.PopStackData()

		self.Jumpifneq(label, data1, data2)

	#
	def Dprint(self):
		...
	def Break(self):
		...

