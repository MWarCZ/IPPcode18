
class function(object):
	"""Vytvoreni funkce kterou je mozne zavolat pozdeji."""
	def __init__(self, function, params=None):
		self.function = function
		self.params = params

	def call(self):
		if self.params==None:
			return self.function()
		elif type(self.params)!=type((1,'1')) and type(self.params)!=type([1,2]):
			return self.function(self.params)
		else:
			return self.function(*self.params)
