

class Stack(object):
	def __init__(self):
		self.list = []

	def Empty(self):
		return True if len(self.list)==0 else False

	def Push(self, item):
		self.list.append(item)

	def Pop(self):
		return None if self.Empty() else self.list.pop()

	def Top(self):
		return None if self.Empty() else self.list[len(self.list)-1]


class Frame(object):
	def __init__(self):
		self.dict = {}

	def ExistVar(self, name):
		return name in self.dict.keys()

	def GetVar(self, name):
		return self.dict[name] if self.ExistVar(name) else None

	def SetVar(self, name, value):
		self.dict[name] = value
