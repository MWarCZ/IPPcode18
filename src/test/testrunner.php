<?php
/* -/\/\-\/\/- */

require_once('errors.php');

require_once('debug.php');

class TestRunner {
  private $php_interpret;
  private $python_interpret;
  private $int_script;
  private $parse_script;

  //
  public function __construct($parse_script, $int_script ,$php_interpret = "php5.6", $python_interpret = "python3.6") {
    $this->php_interpret = $php_interpret;
    $this->python_interpret = $python_interpret;
    $this->parse_script = $parse_script;
    $this->int_script = $int_script;
  }
  //
  public function TestParse($parse_script, $filename_src, $filename_tmp) {
    $output = array();
    $ret_val = 0;

    $php_interpret = $this->php_interpret;
    exec("$php_interpret $parse_script < $filename_src ;", $output, $ret_val);

    $temp = fopen($filename_tmp, "w");
    foreach ($output as $key => $value) {
      fwrite($temp, $value );
    }
    fclose($temp);

    return $ret_val;
  }
  //
  public function TestInterpret($int_script, $filename_in, $filename_tmp ) {
    $output = array();
    $ret_val = 0;

    $python_interpret = $this->python_interpret;
    exec("$python_interpret $int_script --source=$filename_tmp < $filename_in ;", $output, $ret_val);

    $temp = fopen($filename_tmp, "w");
    foreach ($output as $key => $value) {
      fwrite($temp, $value );
    }
    fclose($temp);

    return $ret_val;
  }
  //
  public function ReadDir($dir_path = ".", &$files = array(), &$dirs = array() ) {
    $dir_path = rtrim($dir_path,DIRECTORY_SEPARATOR);
    if(is_dir($dir_path)) {
      $list = scandir($dir_path);
      foreach ($list as $key => $value) {
        $path = "$dir_path".DIRECTORY_SEPARATOR."$value";
        if( is_dir($path) ) {
          if($value != "." and $value != "..") {
            array_push($dirs, $path);
          }
        }
        else if( is_file($path) ) {
          array_push($files, $path);
        }
      } // foreach
    } 
    else {
      error_log("Slozka '$dir_path' neexistuje.");
      exit(Errors::ERR_INPUT_FILE);
    }
  }
  //
  // src - kod IPPcode18
  // in  - Vstup interpretu stdin
  // out - vystup interpretu stdout
  // rc  - navratovy kod
  public function CheckFiles($files = array(), &$files_src = array(), &$files_in = array(), &$files_out = array(), &$files_rc = array() ) {
    foreach ($files as $key => $value) {
      if(preg_match("/.(src|in|out|rc)$/", $value )) {
        $file_part = preg_replace("/.(src|in|out|rc)$/", "", $value );
        //debug("OK: $value\nfp: $file_part\n");
        $file_src = $file_part . ".src";
        $file_in = $file_part . ".in";
        $file_out = $file_part . ".out";
        $file_rc = $file_part . ".rc";

        //debug("--------------\n$file_src\n$file_in\n$file_out\n$file_rc\n-------------\n");

        if(!file_exists($file_src)) {
          $tmp = fopen($file_src,"w");
          fclose($tmp); 
        }
        array_push($files_src, $file_src);
        if(!file_exists($file_in)) {
          $tmp = fopen($file_in,"w");
          fclose($tmp); 
        }
        array_push($files_in, $file_in);
        if(!file_exists($file_out)) {
          $tmp = fopen($file_out,"w");
          fclose($tmp); 
        }
        array_push($files_out, $file_out);
        if(!file_exists($file_rc)) {
          $tmp = fopen($file_rc,"w");
          fwrite($tmp,"0");
          fclose($tmp); 
        }
        array_push($files_rc, $file_rc);

      }
      else {
        //debug("KO: $value\n");
      }
    } // foreach
  }
  //
  public function Test($filename_src, $filename_in) {
    $parse_script = $this->parse_script; 
    $int_script = $this->int_script;

    $ret_val = 0;
    $filename_tmp = tempnam("/tmp", "tmp-ipp-");

    $ret_val = $this->TestParse($parse_script, $filename_src, $filename_tmp);

    if($ret_val == 0) {
      $ret_val = $this->TestInterpret($int_script, $filename_in, $filename_tmp );
    }
    // Smaze soubor
    unlink($filename_tmp); 

    return $ret_val;
  }
  //

}

?>
