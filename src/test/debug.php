<?php
/* -/\/\-\/\/- */

function debug($msg = "") {
  //error_log($msg);
  file_put_contents('php://stderr', print_r($msg, TRUE));
}

function debug_var_dump($obj,$msg="") {
  ob_start();
  var_dump($obj);
  $result = ob_get_clean();
  debug("$msg$result");
}

?>
