<?php
/* -/\/\-\/\/- */

require_once('errors.php');

class HTMLGenerator {
  public $dom = null;
  public $html = null;
  public $main_table = null;

  public function __construct() {
    $this->dom = new DomDocument("1.0", "UTF-8");

    $this->html = $this->dom->createElement('html');
    $this->dom->appendChild($this->html);

      $head = $this->dom->createElement('head');
      $this->html->appendChild($head);

        $title = $this->dom->createElement('title','Testy');
        $head->appendChild($title);

        $css="
        .ko {
          background: #ff8383;
        }
        .ok {
          background: #59ff59;
        }
        body table {
          width: 100%;
          background: #000;
          font-weight: bold;
          border-collapse: collapse;
        }
        body > table {
          border: black 1px solid;
        }
        table, td, tr, thead, tbody{
          padding: 0;
          margin: 0;
        }
        td {
          border: 1px black solid;
        }
        a {
          color: black;
        }

        ";
        $style = $this->dom->createElement('style',$css);
        $head->appendChild($style);

      $body = $this->dom->createElement('body');
      $this->html->appendChild($body);

        $this->main_table = $this->dom->createElement('table');
        $body->appendChild($this->main_table);

  }

  public function AddTestOld($table,$filename,$isok=false) {
    $tr = $this->dom->createElement('tr');
    $table->appendChild($tr);

    $td = $this->dom->createElement('td',"$filename");
    if($isok) {
      $td->setAttribute('class','ok');
    }
    else {
      $td->setAttribute('class','ko');
    }
    $tr->appendChild($td);

    return $td;
  }

  public function AddTest($table,$filename,$isok=false) {
    $tr = $this->dom->createElement('tr');
    $table->appendChild($tr);

    $td = $this->dom->createElement('td');
    $tr->appendChild($td);

    $a = $this->dom->createElement('a',"$filename");
    $a->setAttribute('href',"$filename");
    $td->appendChild($a);

    if($isok) {
      $td->setAttribute('class','ok');
    }
    else {
      $td->setAttribute('class','ko');
    }

    return $td;
  }

  public function AddDir($table,$dir) {
    $tr = $this->dom->createElement('tr');
    $table->appendChild($tr);

    $td_d = $this->dom->createElement('td',"$dir");
    $tr->appendChild($td_d);

    $td_t = $this->dom->createElement('td');
    $tr->appendChild($td_t);

    $new_table = $this->dom->createElement('table');
    $td_t->appendChild($new_table);

    return array($td_d, $new_table);
  }
  public function Set($elem,$isok=false) {
    if($isok) {
      $elem->setAttribute('class','ok');
    }
    else {
      $elem->setAttribute('class','ko');
    }
    return $elem;
  }

  private function EscapeString($val = "") {
    return htmlspecialchars($val, ENT_XML1 | ENT_COMPAT, 'UTF-8');
    //return htmlspecialchars($val, ENT_XML1 | ENT_QUOTES, 'UTF-8');
  }

  public function PrintXML($formatOutput = false) {
    $this->dom->formatOutput = $formatOutput;
    $str = $this->dom->saveXML();
    echo "$str";
  }
  public function PrintHTML($formatOutput = false) {
    $this->dom->formatOutput = $formatOutput;
    $str = $this->dom->saveHTML();
    echo "$str";
  }

}


?>
