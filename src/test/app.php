<?php
/* -/\/\-\/\/- */

require_once('errors.php');

require_once('testrunner.php');

require_once('debug.php');

require_once('htmlgenerator.php');

class App {

  /**
   * Uchovava pole argumentu, ktere odpovidaji argumentum na prikazove radce.
   * @var array
   */
  private $args = array();

  /**
   * Konstruktor
   * @param array $args Pole argumentu prikazove radky. Dle techto argumentu se nastavi vlastnosti. Ocekava se $argv ci shodny format.
   */
  function __construct($args = array()) {
    $this->args = $args;
  }

  /**
   * Funkce znormalizuje format argumentu prikazove radky.
   * Odstrani nazev programu (index 0), prevede ['--xxx=aaa'] na ['--xxx', 'aaa']
   * @param array $args Pole argumentu prikazove radky. Ocekava se $argv ci shodny format.
   * @return array Vraci upravene pole, ktere obsahuje jen argumenty a hodnoty.
   */
  public function NormalizeOpts($args = array()) {
    $out_args = array();
    foreach ($this->args as $key => $value) {
      if($key != 0) {
        if( preg_match("/^(--|-)/",$value) ) {
          $arr = preg_split("/=/",$value,2);
          array_push($out_args, $arr[0]);
          if( @$arr[1] )
            array_push($out_args, $arr[1]);
        }
        else {
          array_push($out_args, $value);
        }
      }
    } // foreach
    return $out_args;
  }

  /**
   * Hlavni funkce, ktera spusti aplikaci.
   * Zpracuje svoje argumenty zadane pri vytvareni a dle jejich hodnot upravi chovani aplikace.
   */
  public function Main() {

    $args = $this->NormalizeOpts($this->args);

    // if($this->CheckDuplicity($args)) {
    //   error_log("Byly zadany nektere argumeny duplicitne.\n");
    //   exit(Errors::ERR_INPUT_PARAM);
    // }

    $parse_script = "";
    $int_script = "";
    $directory = array();
    $recursive = false;
    $load_value = "";

    // --help  --directory=path  --recursive
    // --parse-script=file  --int-script=file
    // FILES: --testlist=file (nelze komb s --directory)
    foreach ($args as $key => $value) {
      if( $load_value != "" ) {
        if( $load_value == "directory" ) {
          ${$load_value}[count($$load_value)] = $value;
        }
        else {
          $$load_value = $value;
        }
        $load_value = "";
      }
      else if( "--help" == $value ) {
        if( count($args) != 1 ) {
          error_log("Argument '--help' nelze kombinovat s jinymi argumenty.\n");
          exit(Errors::ERR_INPUT_PARAM);
        }
        $this->Help();
        return;
      }
      else if( "--recursive" == $value ) {
        if($recursive) {
          error_log("Argument '--recursive' byl zadan duplicitne.\n");
          exit(Errors::ERR_INPUT_PARAM);
        }
        $recursive = true;
      }
      else if( "--parse-script" == $value ) {
        if($parse_script) {
          error_log("Argument '--parse-script' byl zadan duplicitne. Muze byt testovan jen jeden parse skript.\n");
          exit(Errors::ERR_INPUT_PARAM);
        }
        $load_value = "parse_script";
      }
      else if( "--int-script" == $value ) {
        if($int_script) {
          error_log("Argument '--int-script' byl zadan duplicitne. Muze byt testovan jen jeden interpret.\n");
          exit(Errors::ERR_INPUT_PARAM);
        }
        $load_value = "int_script";
      }
      else if( "--directory" == $value ) {
        $load_value = "directory";
      }
      else {
        error_log("Zadany chybne argumenty.\nPomoci argumentu '--help' si vypises napovedu.");
        exit(Errors::ERR_INPUT_PARAM);
      }

    } // foreach
    if( $load_value != "" ) {
      error_log("Zadany chybne argumenty.\nPomoci argumentu '--help' si vypises napovedu.");
      exit(Errors::ERR_INPUT_PARAM);
    }

    /// DEBUG
    debug(">ps: $parse_script\n>is: $int_script\n");
    debug(">rec: $recursive\n>lv: $load_value\n");
    debug_var_dump($directory,">dir: ");

    //$this->xxx();
    $testrunner = new TestRunner($parse_script,$int_script);
    $htmlgen = new HTMLGenerator();

    $files_src = $this->PathArray($testrunner, $directory, $recursive);
    $tree = $this->Tree($files_src);
    debug_var_dump($tree,">Tree: ");
    $this->ForeachTree($tree, $htmlgen, $htmlgen->main_table, $testrunner);
    $htmlgen->PrintHTML(true);
    
  }

  public function PathArray($testrunner, $directory, $recursive) {
    $files_src = array();
    $files_in = array();
    $files_out = array();
    $files_rc = array();

    for ( $i=0; $i < count($directory); $i++ ) {
      $tmp_files=array(); 
      $tmp_dirs=array();
      $testrunner->ReadDir($directory[$i], $tmp_files, $tmp_dirs);

      /// DEBUG
      // debug_var_dump($tmp_files,">files: ");
      // debug_var_dump($tmp_dirs,">dirs: ");

      $testrunner->CheckFiles($tmp_files, $files_src, $files_in, $files_out, $files_rc);

      if($recursive) {
        $directory = array_merge($directory,$tmp_dirs);
      }

      // debug_var_dump($files_src,">files_src: ");
      // debug_var_dump($files_in,">files_in: ");
      // debug_var_dump($files_out,">files_out: ");
      // debug_var_dump($files_rc,">files_rc: ");

    }
    debug_var_dump($files_src,">files_src: ");

    return $files_src;
  }

  public function Tree($array){
    $tree = array();
    foreach($array AS $path) {
        $dirs = explode("/", $path);
        $current = &$tree;
        foreach($dirs AS $dir) {
            if(!isset($current[$dir])) {
                $current[$dir] = array();
            }
            $current = &$current[$dir];
        }
        $current[0] = $path;
    }
    return $tree;
  }

  public function ForeachTree($array, $htmlgen, $main_table, $testrunner) {
    $all_ok=true;

    foreach ($array as $key => $value) {
      if(!isset($value[0])) {
        // Add dir
        $table = $htmlgen->AddDir($main_table, $key);
        $is_ok = $this->ForeachTree($value, $htmlgen, $table[1], $testrunner);
        $all_ok = ($all_ok and $is_ok);

        $htmlgen->Set($table[0],$is_ok);
      }
      else {

        // Run test parse ...
        // Run test interpret ...
        $files_src = array();
        $files_in = array();
        $files_out = array();
        $files_rc = array();
        $testrunner->CheckFiles($value, $files_src, $files_in, $files_out, $files_rc);
        $ret_val = $testrunner->Test($files_src[0],$files_in[0]);
        
        // prozatimni prvek nahody uspesnosti testu.
        //$bbb = array(true, true,true, true,false);
        //$is_ok = $bbb[rand(0,4)];
        $is_ok = ($ret_val == 0)?true:false;
        $all_ok = ($all_ok and $is_ok);

        // Add test to html
        $test = $htmlgen->AddTest($main_table, $value[0], $is_ok);
      }
      
    }
    return $all_ok;
  }

  private $php_interpret = "php5.6";
  private $python_interpret = "python3.6";
  // parse.php < file.ippcode18
  // interpret.py --source=file
  // file.(src|in|out|rc)
  // src - kod IPPcode18
  // in  - Vstup interpretu stdin
  // out - vystup interpretu stdout
  // rc  - navratovy kod
  private function xxx($parse_script = "./parse.php", $file_in = "./aaa") {

    $output = array();
    $ret_val = 0;
    exec("php5.6 $parse_script < $file_in", $output, $ret_val);

    // Jedinecne jmeno pro tmp file
    $tmpfname = tempnam("/tmp", "tmp-");
    debug(">tmpname: $tmpfname\n");

    $temp = fopen($tmpfname, "w");
    foreach ($output as $key => $value) {
      fwrite($temp, $value );
    }
    fclose($temp);

    // Smaze soubor
    unlink($tmpfname); 
  }

  /**
   * Funkce vypise napovedu k aplikaci na standartni vystup.
   */
  public function Help() {
    $name = $this->args[0];

    echo "Popis: \n";
    echo " Skrypt zpracovava kod IPPcode18 ze standartniho vstupu, provede lexikalni a syntaktickou kontrolu,a vygeneruje z nej XML dokument.\n";
    echo "\n";

    echo "Pouziti: '$name [prepinace]'\n";
    echo "\n";

    echo "Prepinace: \n";
    echo " --help\t\tVypise napovedu k programu.\n";
    echo " --stats=file\tDo souboru 'file' se ulozi zvolene statistiky.\n";
    echo " \t\tPrepinas se musi zkobinovat s '--loc' ci '--comments'\n";
    echo " --loc\t\tUlozi se do statistik pocet zpracovanych instrukci.\n";
    echo " --comments\tUlozi se do statistik pocet zpracovanych komentaru.\n";
    echo "\n";

    echo "Kody chyb: \n";
    echo "  10\tChybne zadane prepinace/argumenty nebo chybna kombinace.\n";
    echo "  12\tChyba pri otevirani/vytvareni souboru pro zapis statistik.\n";
    echo "  21\tDoslo k syntaktice/lexikalni chybe ve vstupnim kodu IPPcode18.\n";
    echo "  99\tInterni chyba.\n";
    echo "\n";
  }

}


?>

